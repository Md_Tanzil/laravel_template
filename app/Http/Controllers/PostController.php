<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        $post = Post::latest()->get();
        // dd($post);
        return view('backend.post', [
            'post' => $post
        ]);
    }
    public function create()
    {
        return view('backend.post.create');
    }
    public function store(Request $request)
    {

        // dd(request()->all());
        Post::create([
            'title' => $request->title,
            'description' => $request->description,

        ]);
        $request->session()->flash('message', 'Add successful!');
        return redirect()->route('post');

    }
}