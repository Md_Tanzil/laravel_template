<x-backend.layouts.master>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7">
                <div class="card shadow-lg border-0 rounded-lg mt-5">
                    <div class="card-header">
                        <h3 class="text-center font-weight-light my-4">Create Post</h3>
                    </div>
                    <div class="card-body">
                        <form action="{{route('post.store')}}" method="POST">
                            @csrf
                            <div class="row mb-6">
                                <div class="col-md-12">
                                    <div class="form-floating mb-3 mb-md-0">
                                        <input class=" form-control" name="title" id="inputFirstName" type="text" placeholder="Enter your Title " />
                                        <label for="inputFirstName">Title</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-floating">
                                        <input class="form-control" name="description" id="inputLastName" type="textbox" placeholder="Enter your Description" />
                                        <label for="inputLastName">Description</label>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary mt-2">Add Post</button>
                            </div>


                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <x-slot name='title'>Create Post</x-slot>
</x-backend.layouts.master>