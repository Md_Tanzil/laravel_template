<x-backend.layouts.master>

    <div class="container-fluid px-4">

        <div class="card mb-4">
            <div class="card-body">
                DataTables is a third party plugin that is used to generate the demo table below. For more information
                about DataTables, please visit the
                <a target="_blank" href="https://datatables.net/">official DataTables documentation</a>
                .
            </div>
        </div>
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table me-1"></i>
                DataTable Example
            </div>
            <div class="card-body">
                @if (request()->session()->has('message'))
                    <div class="alert alert-success" role="alert">
                     {{ session('message')}}
                   </div>
                @endif
                    
        
                <table id="datatablesSimple">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Title</th>
                            <th>Description</th>

                    </tfoot>

                    <tbody>
                        @foreach($post as $posts)
                        <tr>
                            <td>{{$posts->title}}</td>
                            <td>{{$posts->description}}</td>

                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <x-slot name=title>
        Post
    </x-slot>
</x-backend.layouts.master>